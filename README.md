# Getting Started

### Config development env
Copy default configuration from .env.example and save as .env
```
cp .env.example .env
```

### Install dependencies

```
yarn
```

### Start app

```
yarn start
```
