import React, { useEffect } from 'react';
import { Route, Switch, useHistory, useLocation } from 'react-router-dom';
import './App.css';
import ListInvoice from './pages/list-invoice';
import Login from './pages/login';
import CreateInvoice from './pages/create-invoice';
import { Container, Box, CircularProgress } from '@mui/material';
import useAuthenticated from './hooks/useAuthenticated';
import { ROUTES } from './consts';
import useApi from './hooks/useApi';
import useUser from './hooks/useUser';

function App() {
  const history = useHistory();
  const location = useLocation();
  const authenticated = useAuthenticated();
  const { orgToken } = useUser();
  const { fetch, loading, success } = useApi({
    endpoint: 'https://sandbox.101digital.io/membership-service/1.2.0/users/me',
    actionType: 'ACCOUNT/getMe',
  });

  if (!authenticated && location.pathname !== ROUTES.login) {
    history.push(ROUTES.login);
  }

  if (authenticated && location.pathname === ROUTES.login) {
    history.push(ROUTES.home);
  }

  useEffect(() => {
    if (authenticated && !loading && !success) fetch();
  }, [authenticated]);

  if (authenticated && !orgToken) {
    return (
      <Box
        sx={{
          p: 2,
          m: 2,
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <CircularProgress size={100} />
      </Box>
    );
  }

  return (
    <div className="App">
      <Container maxWidth="lg">
        <Box sx={{ p: 2, m: 2 }}>
          <Switch>
            <Route exact path={ROUTES.listInvoice} component={ListInvoice} />
            <Route path={ROUTES.login} component={Login} />
            <Route path={ROUTES.createInvoice} component={CreateInvoice} />
          </Switch>
        </Box>
      </Container>
    </div>
  );
}

export default App;
