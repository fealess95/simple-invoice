import React, { useEffect } from 'react';
import qs from 'qs';
import useApi from '../../hooks/useApi';
import InvoiceTable, { InvoiceTablePlaceholder } from './components/Table';
import { Button, Grid } from '@mui/material';
import { ROUTES } from '../../consts';
import { useHistory } from 'react-router-dom';
import FilterBlock from './components/FilterBlock';
import TablePagination from './components/TablePagination';
import { useQuery } from './hooks/useQuery';
import useUser from '../../hooks/useUser';

const API_ENDPOINT =
  'https://sandbox.101digital.io/invoice-service/1.0.0/invoices';

function ListInvoice() {
  const history = useHistory();
  const { orgToken } = useUser();
  const { keyword, ordering, fromDate, toDate, pageSize, pageNum } = useQuery();

  const { fetch, loading, data } = useApi({
    endpoint: API_ENDPOINT,
    actionType: 'LIST_INVOICE/get',
  });

  useEffect(() => {
    const query = qs.stringify({
      keyword,
      ordering,
      fromDate,
      toDate,
      pageSize,
      pageNum: +pageNum + 1,
      dateType: 'INVOICE_DATE',
      sortBy: 'CREATED_DATE',
    });
    fetch({
      endpoint: API_ENDPOINT + `?${query}`,
      headers: {
        'org-token': orgToken,
      },
    });
  }, [keyword, ordering, fromDate, toDate, pageSize, pageNum]);

  const onClickCreate = () => {
    history.push(ROUTES.createInvoice);
  };

  return (
    <Grid container spacing={2} justifyContent="flex-end">
      <Grid item xs={8} sm={4} md={3} container justifyContent="center">
        <Button
          variant="contained"
          onClick={onClickCreate}
          style={{ float: 'right' }}
          fullWidth
        >
          Create Invoice
        </Button>
      </Grid>
      <Grid item xs={12} container>
        <FilterBlock />
      </Grid>
      {loading && (
        <Grid item xs={12} container justifyContent="center">
          <InvoiceTablePlaceholder />
        </Grid>
      )}
      {!!data?.data && !loading && (
        <>
          <Grid item xs={12} container justifyContent="center">
            <InvoiceTable data={data.data} />
          </Grid>
          <Grid item xs={12} container justifyContent="center">
            <TablePagination count={data.paging.totalRecords} />
          </Grid>
        </>
      )}
    </Grid>
  );
}

export default ListInvoice;
