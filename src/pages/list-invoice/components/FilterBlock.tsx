import React, { useMemo, useState } from 'react';
import { Grid, MenuItem, Select, TextField } from '@mui/material';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { useQuery } from '../hooks/useQuery';
import debounce from 'lodash/debounce';

const FilterBlock = () => {
  const query = useQuery();
  const {
    keyword,
    setKeyword,
    ordering,
    setOrdering,
    fromDate,
    setFromDate,
    toDate,
    setToDate,
  } = query;
  const [value, setValue] = useState(keyword);

  const handleDebounceFn = (value: string) => {
    setKeyword(value);
  };

  const debounceFn = useMemo(() => debounce(handleDebounceFn, 500), [query]);

  const handleChange = (event: any) => {
    setValue(event.target.value);
    debounceFn(event.target.value);
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <Grid container spacing={2} justifyContent="space-between">
        <Grid item xs={6} sm={3}>
          <TextField
            label="Search"
            fullWidth
            value={value}
            onChange={handleChange}
            size="small"
          />
        </Grid>
        <Grid item xs={6} sm={3}>
          <Select
            value={ordering}
            // @ts-ignore
            onChange={e => setOrdering(e.target.value)}
            fullWidth
            size="small"
          >
            <MenuItem value={'ASCENDING'}>ASC</MenuItem>
            <MenuItem value={'DESCENDING'}>DESC</MenuItem>
          </Select>
        </Grid>
        <Grid item xs={6} sm={3}>
          <DatePicker
            // @ts-ignore
            value={fromDate ? new Date(fromDate) : undefined}
            // @ts-ignore
            onChange={newValue => setFromDate(newValue)}
            label="From Date"
            className="fullWidth"
            slotProps={{
              textField: {
                size: 'small',
              },
            }}
          />
        </Grid>
        <Grid item xs={6} sm={3}>
          <DatePicker
            // @ts-ignore
            value={toDate ? new Date(toDate) : undefined}
            // @ts-ignore
            onChange={newValue => setToDate(newValue)}
            label="To Date"
            className="fullWidth"
            slotProps={{
              textField: {
                size: 'small',
              },
            }}
          />
        </Grid>
      </Grid>
    </LocalizationProvider>
  );
};

export default FilterBlock;
