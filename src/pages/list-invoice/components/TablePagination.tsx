import React from 'react';
import { TablePagination as BaseTablePagination } from '@mui/material';
import { useQuery } from '../hooks/useQuery';

const TablePagination = ({ count }: { count: number }) => {
  const { pageNum, setPageNum, pageSize, setPageSize } = useQuery();

  const handleChangePage = (
    event: React.MouseEvent<HTMLButtonElement> | null,
    newPage: number
  ) => {
    setPageNum(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setPageSize(parseInt(event.target.value, 10));
  };

  return (
    <BaseTablePagination
      rowsPerPageOptions={[10, 20, 50]}
      colSpan={3}
      count={count}
      rowsPerPage={+pageSize}
      page={+pageNum}
      onPageChange={handleChangePage}
      onRowsPerPageChange={handleChangeRowsPerPage}
      style={{ float: 'right', width: '100%', borderBottom: 'none' }}
    />
  );
};

export default TablePagination;
