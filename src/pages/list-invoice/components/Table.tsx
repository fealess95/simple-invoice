import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Skeleton from '@mui/material/Skeleton';
import format from 'date-fns/format';

const InvoiceTable = ({ data }: { data: any }) => {
  return (
    <BaseInvoiceTable>
      {data.map((row: any) => (
        <TableRow
          key={row.invoiceId}
          sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
        >
          <TableCell component="th" scope="row">
            {row.referenceNo}
          </TableCell>
          <TableCell>{format(new Date(row.createdAt), 'yyyy-MM-dd')}</TableCell>
          <TableCell>{row.invoiceDate}</TableCell>
          <TableCell>{row.description}</TableCell>
          <TableCell align="right">{row.totalAmount}</TableCell>
        </TableRow>
      ))}
    </BaseInvoiceTable>
  );
};

const BaseInvoiceTable = ({ children }: any) => (
  <TableContainer component={Paper}>
    <Table sx={{ minWidth: 650 }} aria-label="simple table">
      <colgroup>
        <col width="20%" />
        <col width="20%" />
        <col width="20%" />
        <col width="25%" />
        <col width="15%" />
      </colgroup>
      <TableHead>
        <TableRow>
          <TableCell>Invoice No</TableCell>
          <TableCell>Created At</TableCell>
          <TableCell>Invoice Date</TableCell>
          <TableCell>Description</TableCell>
          <TableCell align="right">Amount</TableCell>
        </TableRow>
      </TableHead>
      {children}
    </Table>
  </TableContainer>
);

export const InvoiceTablePlaceholder = ({ length = 10 }) => {
  return (
    <BaseInvoiceTable>
      <TableBody>
        {Array(length)
          .fill({})
          .map((row: any, index) => (
            <TableRow
              key={index}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell>
                <Skeleton variant="text" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" />
              </TableCell>
              <TableCell>
                <Skeleton variant="text" />
              </TableCell>
              <TableCell align="right">
                <Skeleton variant="text" />
              </TableCell>
            </TableRow>
          ))}
      </TableBody>
    </BaseInvoiceTable>
  );
};

export default InvoiceTable;
