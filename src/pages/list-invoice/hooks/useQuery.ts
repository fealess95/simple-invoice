import qs from 'qs';
import { useLocation, useHistory } from 'react-router-dom';
import format from 'date-fns/format';

export const useQuery = () => {
  const location = useLocation();
  const history = useHistory();
  const query = qs.parse(location.search, {
    ignoreQueryPrefix: true,
  });
  const { keyword, ordering, fromDate, toDate, pageSize, pageNum } = query;

  const pushQuery = (newQuery: any) => {
    const queryObj = {
      ...query,
      ...newQuery,
    };
    history.push({
      pathname: location.pathname,
      search: '?' + qs.stringify(queryObj),
    });
  };

  const setKeyword = (keyword: string) => {
    pushQuery({
      keyword,
    });
  };

  const setOrdering = (ordering: string) => {
    pushQuery({
      ordering,
    });
  };

  const setFromDate = (fromDate: Date) => {
    pushQuery({
      fromDate: format(fromDate, 'yyyy-MM-dd'),
    });
  };

  const setToDate = (toDate: Date) => {
    pushQuery({
      toDate: format(toDate, 'yyyy-MM-dd'),
    });
  };

  const setPageSize = (pageSize: number) => {
    pushQuery({
      pageSize,
      pageNum: 0,
    });
  };

  const setPageNum = (pageNum: number) => {
    pushQuery({
      pageNum,
    });
  };

  return {
    keyword,
    ordering: ordering || 'DESCENDING',
    fromDate,
    toDate,
    pageSize: pageSize || 10,
    pageNum: pageNum || 0,
    setKeyword,
    setOrdering,
    setFromDate,
    setToDate,
    setPageSize,
    setPageNum,
  };
};
