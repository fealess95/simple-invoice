import React, { useEffect } from 'react';
import { Box, Button, Grid, TextField, Typography } from '@mui/material';
import { useForm, Controller } from 'react-hook-form';
import { LoginInterface } from './interfaces';
import { getErrorProps } from '../../utils/form';
import useApi from '../../hooks/useApi';
import { ROUTES } from '../../consts';
import { useHistory } from 'react-router-dom';
import useAuthenticated from '../../hooks/useAuthenticated';

function Login() {
  const history = useHistory();
  const authenticated = useAuthenticated();
  const { fetch, loading, success } = useApi({
    endpoint: 'https://sandbox.101digital.io/token?tenantDomain=carbon.super',
    actionType: 'LOGIN/login',
  });

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      username: '',
      password: '',
    },
  });

  const onSubmit = (data: LoginInterface) => {
    let formBody: string[] = [];
    const body = {
      client_id: process.env.REACT_APP_LOGIN_CLIENT_ID,
      client_secret: process.env.REACT_APP_LOGIN_CLIENT_SECRET,
      grant_type: 'password',
      scope: 'openid',
      username: data.username,
      password: data.password,
    };

    Object.entries(body).forEach(([key, value]) => {
      const encodedKey = encodeURIComponent(key);
      const encodedValue = encodeURIComponent(value ?? '');
      formBody = [...formBody, encodedKey + '=' + encodedValue];
    });

    fetch({
      method: 'POST',
      body: formBody.join('&'),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    });
  };

  useEffect(() => {
    if (success) {
      history.push(ROUTES.home);
    }
  }, [success, history]);

  useEffect(() => {
    if (authenticated) {
      history.push(ROUTES.home);
    }
  }, []);

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Box>
        <Grid container justifyContent="center">
          <Grid item xs={12} sm={9} md={6} container spacing={2}>
            <Grid item xs={12} container justifyContent="center">
              <Typography variant="h4">Login</Typography>
            </Grid>
            <Grid item xs={12}>
              <Controller
                name="username"
                control={control}
                rules={{
                  required: 'username is required',
                  pattern: {
                    value:
                      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                    message: 'Please enter a valid email',
                  },
                }}
                render={({ field }) => (
                  <TextField
                    {...field}
                    {...getErrorProps(errors['username'])}
                    label="username"
                    fullWidth
                    InputLabelProps={{ shrink: true }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12}>
              <Controller
                name="password"
                control={control}
                rules={{
                  required: 'password is required',
                  pattern: {
                    value: /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\W)/,
                    message:
                      'The password must include lower case, upper case, digit, and symbol',
                  },
                }}
                render={({ field }) => (
                  <TextField
                    {...field}
                    {...getErrorProps(errors['password'])}
                    label="password"
                    fullWidth
                    type="password"
                    InputLabelProps={{ shrink: true }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12}>
              <Button
                variant="contained"
                fullWidth
                type="submit"
                disabled={loading}
              >
                Login
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </form>
  );
}

export default Login;
