import format from 'date-fns/format';

const randomInt = (min: number, max: number) =>
  Math.floor(Math.random() * (max - min)) + min;

const randomItem = (items: string[]) =>
  items[Math.floor(Math.random() * items.length)];

const randomRef = () =>
  Math.random().toString(36).substring(2, 10).toUpperCase();

export const genInvoicePayload = ({
  invoiceReference,
  invoiceDate,
  description,
}: {
  invoiceDate: Date;
  invoiceReference?: string;
  description?: string;
}) => {
  return {
    invoices: [
      {
        invoiceReference,
        invoiceDate: format(invoiceDate, 'yyyy-MM-dd'),
        description,
        items: [
          {
            itemReference: randomRef(),
            description: randomItem([
              '911 Carrera',
              '911 Carrera Cabrio',
              '911 Targa',
              '911 Turbo',
              '924',
              '944',
              '997',
              'Boxster',
              'Cayenne',
              'Cayman',
              'Macan',
              'Panamera',
            ]),
            quantity: randomInt(1, 200),
            rate: 1000,
            itemName: 'Honda Motor',
            itemUOM: 'KG',
            customFields: [
              {
                key: 'taxiationAndDiscounts_Name',
                value: 'VAT',
              },
            ],
            extensions: [
              {
                addDeduct: 'ADD',
                value: 10,
                type: 'FIXED_VALUE',
                name: 'tax',
              },
              {
                addDeduct: 'DEDUCT',
                value: 10,
                type: 'PERCENTAGE',
                name: 'tax',
              },
            ],
          },
        ],
        invoiceNumber: 'INV12345670333',
        currency: 'GBP',
        dueDate: '2021-06-04',
        customFields: [
          {
            key: 'invoiceCustomField',
            value: 'value',
          },
        ],
        extensions: [
          {
            addDeduct: 'ADD',
            value: 10,
            type: 'PERCENTAGE',
            name: 'tax',
          },
          {
            addDeduct: 'DEDUCT',
            type: 'FIXED_VALUE',
            value: 10,
            name: 'discount',
          },
        ],
        bankAccount: {
          bankId: '',
          sortCode: '09-01-01',
          accountNumber: '12345678',
          accountName: 'John Terry',
        },
        customer: {
          firstName: 'Nguyen',
          lastName: 'Dung 2',
          contact: {
            email: 'nguyendung2@101digital.io',
            mobileNumber: '+6597594971',
          },
          addresses: [
            {
              premise: 'CT11',
              countryCode: 'VN',
              postcode: '1000',
              county: 'hoangmai',
              city: 'hanoi',
            },
          ],
        },
        documents: [
          {
            documentId: '96ea7d60-89ed-4c3b-811c-d2c61f5feab2',
            documentName: 'Bill',
            documentUrl: 'http://url.com/#123',
          },
        ],
      },
    ],
  };
};
