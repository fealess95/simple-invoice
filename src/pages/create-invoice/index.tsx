import React from 'react';
import {
  Alert,
  AlertColor,
  Box,
  Button,
  Grid,
  Snackbar,
  TextField,
  Typography,
} from '@mui/material';
import { Controller, useForm } from 'react-hook-form';
import { getErrorProps } from '../../utils/form';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import useApi from '../../hooks/useApi';
import { genInvoicePayload } from './utils';
import useUser from '../../hooks/useUser';
import { useHistory } from 'react-router-dom';
import { ROUTES } from '../../consts';

function CreateInvoice() {
  const history = useHistory();
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      invoiceReference: undefined,
      invoiceDate: undefined,
      description: undefined,
      // totalAmount: undefined,
    },
  });
  const { orgToken } = useUser();
  const { fetch, loading } = useApi({
    endpoint: 'https://sandbox.101digital.io/invoice-service/2.0.0/invoices',
    actionType: 'CREATE_INVOICE/save',
  });
  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const [alert, setAlert] = React.useState({
    severity: 'success',
    message: '',
  });

  const handleCloseSnackbar = (
    event: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === 'clickaway') return;

    setOpenSnackbar(false);
  };

  const onSubmit = async (data: any) => {
    const { success } = await fetch({
      method: 'POST',
      body: genInvoicePayload({
        invoiceDate: data.invoiceDate,
        invoiceReference: data.invoiceReference,
        description: data.description,
      }),
      headers: {
        'org-token': orgToken,
        // 'Operation-Mode': 'SYNC',
      },
    });

    setOpenSnackbar(true);
    if (success) {
      setAlert({
        severity: 'success',
        message: 'Create invoice successful',
      });
      setTimeout(() => history.push(ROUTES.home), 1000);
    } else {
      setAlert({
        severity: 'error',
        message: 'Create invoice fail',
      });
    }
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={openSnackbar}
        onClose={handleCloseSnackbar}
      >
        <Alert
          onClose={handleCloseSnackbar}
          severity={alert.severity as AlertColor}
        >
          {alert.message}
        </Alert>
      </Snackbar>
      <Box>
        <Grid container justifyContent="center">
          <Grid item xs={12} sm={9} md={6} container spacing={2}>
            <Grid item xs={12} container justifyContent="center">
              <Typography variant="h4">Create Invoice</Typography>
            </Grid>
            <Grid item xs={12}>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <Controller
                  name="invoiceDate"
                  control={control}
                  rules={{
                    required: 'Invoice Date is required',
                  }}
                  render={({ field }) => (
                    <DatePicker
                      {...field}
                      slotProps={{
                        textField: getErrorProps(errors['invoiceDate']),
                      }}
                      label="Invoice Date"
                      className="fullWidth"
                    />
                  )}
                />
              </LocalizationProvider>
            </Grid>
            <Grid item xs={12}>
              <Controller
                name="invoiceReference"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    {...getErrorProps(errors['invoiceReference'])}
                    label="Reference No."
                    fullWidth
                  />
                )}
              />
            </Grid>
            <Grid item xs={12}>
              <Controller
                name="description"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    {...getErrorProps(errors['description'])}
                    label="Description"
                    fullWidth
                  />
                )}
              />
            </Grid>
            {/*<Grid item xs={12}>*/}
            {/*  <Controller*/}
            {/*    name="totalAmount"*/}
            {/*    control={control}*/}
            {/*    render={({ field }) => (*/}
            {/*      <TextField*/}
            {/*        {...field}*/}
            {/*        {...getErrorProps(errors['totalAmount'])}*/}
            {/*        label="Total Amount"*/}
            {/*        fullWidth*/}
            {/*        type="number"*/}
            {/*      />*/}
            {/*    )}*/}
            {/*  />*/}
            {/*</Grid>*/}
            <Grid item xs={12}>
              <Button
                variant="contained"
                fullWidth
                type="submit"
                disabled={loading}
              >
                Save
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </form>
  );
}

export default CreateInvoice;
