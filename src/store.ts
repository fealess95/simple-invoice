import { configureStore } from '@reduxjs/toolkit';
import thunk from 'redux-thunk';
import loginReducer, { loginSlice } from './reducers/login';
import accountReducer, { accountSlice } from './reducers/account';
import listInvoiceReducer, { listInvoiceSlice } from './reducers/list-invoice';
import createInvoiceReducer, {
  createInvoiceSlice,
} from './reducers/create-invoice';

export const store = configureStore({
  reducer: {
    [loginSlice.name]: loginReducer,
    [accountSlice.name]: accountReducer,
    [listInvoiceSlice.name]: listInvoiceReducer,
    [createInvoiceSlice.name]: createInvoiceReducer,
  },
  middleware: [thunk],
});
