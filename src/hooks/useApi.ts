import { useDispatch, useSelector } from 'react-redux';

import callApiAction from '../helpers/callApiAction';

interface Params {
  endpoint: string;
  actionType: string;
}

interface FetchOptions {
  endpoint?: string;
  method?: string;
  body?: any;
  headers?: any;
}

const useApi = (params: Params) => {
  const reducer = params.actionType.split('/')[0];
  const dispatch = useDispatch();
  const state = useSelector((state: any) => state[reducer]);

  const fetch = async (options?: FetchOptions): Promise<any> =>
    await dispatch(
      callApiAction(
        options?.endpoint || params.endpoint,
        params.actionType,
        options
      )
    );

  return {
    fetch,
    loading: state.loading,
    success: state.success,
    data: state.data,
    error: state.error,
  };
};

export default useApi;
