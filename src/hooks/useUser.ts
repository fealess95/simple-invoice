import { useSelector } from 'react-redux';
import { accountSlice } from '../reducers/account';

const useUser = () => {
  const state = useSelector((state: any) => state[accountSlice.name]);
  return {
    orgToken: state?.data?.data?.memberships?.[0]?.token,
  };
};

export default useUser;
