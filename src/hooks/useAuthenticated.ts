import { Cookies } from 'react-cookie';
import { TOKEN_KEY } from '../consts';

const useAuthenticated = () => {
  const cookie = new Cookies();
  return !!cookie.get(TOKEN_KEY);
};

export default useAuthenticated;
