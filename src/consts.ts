export const ROUTES = {
  login: '/login',
  createInvoice: '/create-invoice',
  listInvoice: '/',
  home: '/',
};

export const TOKEN_KEY = 'access_token';
