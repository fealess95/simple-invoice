import { Cookies } from 'react-cookie';
import { TOKEN_KEY } from '../consts';

export interface CallApiAction {
  type: string;
  payload: {
    loading: boolean;
    success: boolean;
    data?: any;
    error?: any;
  };
}

export function callApiAction(
  endpoint: string,
  actionType: string,
  options: any = {
    headers: {},
  }
) {
  const cookie = new Cookies();
  const token = cookie.get(TOKEN_KEY);

  if (options.body) {
    if (
      typeof options.body === 'object' &&
      !(options.body instanceof FormData)
    ) {
      options.headers = {
        'Content-Type': 'application/json',
        ...options.headers,
      };
      options.body = JSON.stringify(options.body);
    }
  }

  options.headers = {
    Authorization: `Bearer ${token}`,
    ...options.headers,
  };

  return async (dispatch: any) => {
    dispatch({
      type: actionType,
      payload: {
        success: false,
        loading: true,
      },
    });
    return await fetch(endpoint, options)
      .then(parse)
      .then(async response => {
        dispatch({
          type: actionType,
          payload: {
            data: response.data,
            success: true,
            loading: false,
          },
        });
        return response;
      })
      .catch(error => {
        dispatch({
          type: actionType,
          payload: {
            error: error.error,
            success: false,
            loading: false,
          },
        });
        return error;
      });
  };
}

const ERR_MES_SYS = '500 Internal Server Error';
const ERR_MES_401 = '401 Unauthorized';

async function parse(response: Response): Promise<any> {
  const httpStatusCode = response.status;
  if (httpStatusCode >= 500) {
    throw {
      success: false,
      error: {
        message: ERR_MES_SYS,
        httpStatusCode: httpStatusCode,
      },
    };
  }

  if (httpStatusCode === 401) {
    throw {
      success: false,
      error: {
        message: ERR_MES_401,
        httpStatusCode: httpStatusCode,
      },
    };
  }

  const json = await response.json();
  if (httpStatusCode < 400) {
    return {
      success: true,
      data: json,
    };
  } else {
    throw {
      success: false,
      error: json,
    };
  }
}

export default callApiAction;
