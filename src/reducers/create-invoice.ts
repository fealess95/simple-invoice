import { createSlice } from '@reduxjs/toolkit';
import { CallApiAction } from '../helpers/callApiAction';

export const createInvoiceSlice = createSlice({
  name: 'CREATE_INVOICE',
  initialState: {
    data: {},
  },
  reducers: {
    save: (state: any, action: CallApiAction) => {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
});

export const { save } = createInvoiceSlice.actions;

// Export the slice reducer as the default export
export default createInvoiceSlice.reducer;
