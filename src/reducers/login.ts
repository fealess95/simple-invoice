import { createSlice } from '@reduxjs/toolkit';
import { Cookies } from 'react-cookie';
import { CallApiAction } from '../helpers/callApiAction';
import { TOKEN_KEY } from '../consts';

export const loginSlice = createSlice({
  name: 'LOGIN',
  initialState: {
    data: {},
  },
  reducers: {
    login: (state: any, action: CallApiAction) => {
      if (action.payload.success) {
        const cookie = new Cookies();
        cookie.set(TOKEN_KEY, action.payload.data?.access_token, {
          path: '/',
          maxAge: action.payload.data?.expires_in,
        });
      }

      return {
        ...state,
        ...action.payload,
      };
    },
  },
});

export const { login } = loginSlice.actions;

// Export the slice reducer as the default export
export default loginSlice.reducer;
