import { createSlice } from '@reduxjs/toolkit';
import { CallApiAction } from '../helpers/callApiAction';

export const listInvoiceSlice = createSlice({
  name: 'LIST_INVOICE',
  initialState: {
    data: {},
  },
  reducers: {
    get: (state: any, action: CallApiAction) => {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
});

export const { get } = listInvoiceSlice.actions;

// Export the slice reducer as the default export
export default listInvoiceSlice.reducer;
