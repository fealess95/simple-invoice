import { createSlice } from '@reduxjs/toolkit';
import { CallApiAction } from '../helpers/callApiAction';

export const accountSlice = createSlice({
  name: 'ACCOUNT',
  initialState: {
    data: {},
  },
  reducers: {
    getMe: (state: any, action: CallApiAction) => {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
});

export const { getMe } = accountSlice.actions;

// Export the slice reducer as the default export
export default accountSlice.reducer;
