import { FieldError } from 'react-hook-form';

export const getErrorProps = (error?: FieldError) => {
  if (!error) return {};

  return {
    error: true,
    helperText: error.message,
  };
};
